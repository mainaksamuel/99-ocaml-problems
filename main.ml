module Problem01 = struct
  (* Problem 01 - Find the last element of a list *)
  let rec last (xs: 'a list): 'a option =
    match xs with
    | [] -> None
    | [x] -> Some x
    | _ :: rest -> last rest

  let () =
    print_endline "Testing Problem 01";
    assert (last [1;2;3;4;5] = Some(5));
    assert (last [] = None)
end

module Problem02 = struct
  (* Problem 02 - Find the last two elements of a list *)
  let rec last_two (xs: 'a list): ( 'a * 'a ) option =
    match xs with
    | [] -> None
    | [_] -> None
    | [x; y] -> Some (x, y)
    | _ :: rest -> last_two rest

  let () =
    print_endline "Testing Problem 02";
    assert (last_two [1;2;3;4] = Some(3,4));
    assert (last_two [1] = None);
    assert (last_two [] = None)
end

module Problem03 = struct
  (* Problem 03 - Find the K'th element of a list *)
  let rec at (k: int) (xs: 'a list): 'a option =
    match xs with
    | x :: _ when k == 1 -> Some x
    | _ :: rest when k > 1 ->  at (k -1) (rest)
    | _ -> None

  let () =
    print_endline "Testing Problem 03";
    assert (at 3 [1;2;3;4;5] = Some(3));
    assert (at 0 [1;2;3;4;5] = None);
    assert (at 7 [1;2;3;4;5] = None)
end

module Problem04 = struct
  (* Problem 04 - Find the number of elements in a list *)
  let rec length (xs: 'a list): int =
    match xs with
    | [] -> 0
    | _ :: rest ->  1 + length rest

  let length_tail_recursion (xs: 'a list): int =
    let rec length_tail_recursion' (xs: 'a list) (result: int): int=
      match xs with
      | [] -> result
      | _ :: rest ->  length_tail_recursion' (rest) (result + 1)
    in length_tail_recursion' xs 0

  let best_length (xs: 'a list): int =
    let result = ref 0 in
    let ys = ref xs in
    while !ys != [] do
      result := !result + 1;
      ys := List.tl !ys
    done;
    !result

  let () =
    print_endline "Testing Problem 04";
    let test (length: 'a list -> int) =
      assert (length []           = 0);
      assert (length [1]          = 1);
      assert (length [1; 2]       = 2);
      assert (length [1; 2; 3]    = 3);
      assert (length [1; 2; 3; 4] = 4)
    in
    test length;
    test length_tail_recursion;
    test best_length
end

module Problem05 = struct
  (* Problem 05 - Reverse a list *)
  let rev (xs: 'a list): 'a list =
    let rec rev' (xs: 'a list) (acc: 'a list): 'a list =
      match xs with
      | [] -> acc
      | x :: rest -> rev' rest (x :: acc )
    in rev' xs []

  let () =
    print_endline "Testing Problem 05";
    assert(rev [1;2;3;4;5] = [5;4;3;2;1])
end

let () =
  print_endline "Hello, World!"
