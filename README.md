# 99 Ocaml Problems

https://ocaml.org/learn/tutorials/99problems.html

## Quick Start

### Test solutions by running

```console
$ ocaml main.ml
```

### Play With Solutions in REPL

For the best experience wrap ocaml in [rlwrap](https://github.com/hanslub42/rlwrap).

```console
$ rlwrap ocaml
# #use "./main.ml";;
... play with loaded solutions ...
```
